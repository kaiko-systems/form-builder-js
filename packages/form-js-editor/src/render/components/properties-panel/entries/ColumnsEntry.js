import { Default } from '@bpmn-io/form-js-viewer';

import { NumberInputEntry } from '../components';

export default function ColumnsEntry(props) {
  const {
    editField,
    field
  } = props;

  const onInput = (value) => {
    let children = field.children.slice();

    if (value > children.length) {
      while (value > children.length) {
        children.push(Default.create({ _parent: field.id }));
      }
    } else {
      children = children.slice(0, value);
    }

    editField(field, 'children', children);
  };

  const value = field.children.length;

  return (
    <div class="fjs-properties-panel-entry">
      <NumberInputEntry
        id="columns"
        label="Columns"
        onInput={ onInput }
        value={ value }
        min="1"
        max="3" />
    </div>
  );
}
