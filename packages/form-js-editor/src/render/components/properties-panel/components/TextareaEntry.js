import { get } from 'min-dash';

import Textarea from './Textarea';

export default function TextareaEntry(props) {
  const {
    editField,
    field,
    id,
    onChange,
    path,
  } = props;

  const onInput = (value) => {
    if (editField && path) {
      editField(field, path, value);
    } else {
      onChange(value);
    }
  };

  const value = path ? get(field, path, '') : props.value;
  return (
    <div class="fjs-properties-panel-entry">
      <label>Title</label>
      <Textarea id={ id } onInput={ onInput } value={ field.title } />
    </div>
  );
}
