import {
  formFieldClasses,
  safeMarkdown
} from '../Util';

const type = 'ns:inlined,widget:base:LabelWidget';


export default function Text(props) {
  const { field } = props;

  const { title = '' } = field;

  return <div class={ formFieldClasses(type) }>
    <h5> { title } </h5>
  </div>;
}

Text.create = function(options = {}) {
  return {
    title: 'Label',
    ...options
  };
};

Text.type = type;
Text.keyed = false;
