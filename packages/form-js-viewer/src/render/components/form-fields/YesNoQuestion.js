import { useContext } from 'preact/hooks';

import { FormContext } from '../../context';

import Description from '../Description';
import Errors from '../Errors';
import Label from '../Label';

import {
    formFieldClasses,
    prefixId
} from '../Util';

const type = 'ns:question,inlined,widget:base:YesNoQuestion';


export default function YesNoQuestion(props) {
    const {
        errors = [],
        field,
    } = props;

    const {
        title,
    } = field;

    const onChange = ({ target }) => {
        props.onChange({
            field,
            value: target.checked
        });
    };

    const { formId } = useContext(FormContext);

    return <div class={formFieldClasses(type, errors)}>
        <div class="tile tile-centered question t-question px-2">
            <div class="tile-content text-small">{title}</div>
            <div class="tile-action self-centered">
                <button class="btn btn-action s-circle mx-1 btn-link">
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-check" style="" strike-width="2">
                        <polyline points="20 6 9 17 4 12"></polyline>
                    </svg>
                </button>
                <button class="btn btn-action s-circle btn-link bg-gray">
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-x" style="" strike-width="2">
                        <line x1="18" y1="6" x2="6" y2="18"></line><line x1="6" y1="6" x2="18" y2="18"></line>
                    </svg>
                </button>
            </div>
        </div>
        <Errors errors={errors} />
    </div>;
}

YesNoQuestion.create = function (options = {}) {
    return {
        ...options
    };
};

YesNoQuestion.type = type;
YesNoQuestion.label = '';
YesNoQuestion.keyed = true;
YesNoQuestion.emptyValue = false;
