import { useContext } from 'preact/hooks';

import FormField from '../FormField';

import { FormRenderContext } from '../../context';


export default function Default(props) {
  const {
    Children,
    Empty
  } = useContext(FormRenderContext);

  const { field } = props;

  const { children = [] } = field;

  return <Children class="fjs-vertical-layout" field={ field }>
    {
      children.map(childField => {
        return <FormField
          { ...props }
          key={ childField.id }
          field={ childField } />;
      })
    }
    {
      children.length ? null : <Empty />
    }
  </Children>;
}

Default.create = function(options = {}) {
  return {
    children: [],
    ...options
  };
};

Default.type = 'ns:form:base:vessel-form';
Default.keyed = false;
