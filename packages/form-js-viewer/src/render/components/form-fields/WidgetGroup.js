import { useContext } from 'preact/hooks';

import { FormRenderContext } from '../..';

import FormField from '../FormField';

import { FormContext } from '../..';

import {
    formFieldClasses,
} from '../Util';

const type = 'ns:widget:base:WidgetGroup';


export default function WidgetGroup(props) {
    const {
        errors = [],
        field,
    } = props;

    const {
        Children,
        Empty
    } = useContext(FormRenderContext);

    const {
        children = [],
        title,
    } = field;

    const onChange = ({ target }) => {
        props.onChange({
            field,
            value: target.checked
        });
    };

    const { formId } = useContext(FormContext);

    return <div class={formFieldClasses(type, errors)}>
        <div class="p2 card card-shadow">
            <h4 class="m-2">{title}</h4>
        </div>
        <Children class="fjs-drop-inline fjs-vertical-layout" field={field}>
            {
                children.map(childField => {
                    return <FormField
                        {...props}
                        key={childField.id}
                        field={childField} />;
                })
            }
            {
                children.length ? null : <Empty />
            }
        </Children>
    </div>;
}

WidgetGroup.create = function (options = {}) {
    return {
        children: [],
        ...options
    };
};

WidgetGroup.type = type;
WidgetGroup.label = '';
WidgetGroup.keyed = true;
WidgetGroup.emptyValue = false;
