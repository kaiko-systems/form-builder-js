import Button from './form-fields/Button';
import Checkbox from './form-fields/Checkbox';
import Default from './form-fields/Default';
import FormComponent from './FormComponent';
import Number from './form-fields/Number';
import Radio from './form-fields/Radio';
import Select from './form-fields/Select';
import Text from './form-fields/Text';
import Textfield from './form-fields/Textfield';
import YesNoQuestion from './form-fields/YesNoQuestion'
import WidgetGroup from './form-fields/WidgetGroup'

export {
  Button,
  Checkbox,
  Default,
  FormComponent,
  Number,
  Radio,
  Select,
  Text,
  Textfield,
  YesNoQuestion,
  WidgetGroup
};

export const formFields = [
  Button,
  Checkbox,
  Default,
  Number,
  Radio,
  Select,
  Text,
  Textfield,
  YesNoQuestion,
  WidgetGroup
];
