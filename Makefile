FILES := $(shell find packages -type f -name '*.js' | grep -v dist )
DIRS := $(shell find packages -type d | grep -v dist )

build: packages $(DIRS) $(FILES)
	npm run build

install:
	npm install
